Metatag:  Metatag Overheid NL
--------------------
This module adds the extra terms defined in the OWMS by the Dutch government [1] to the
available meta tags. These terms are prefixed in the standard by 'overheid:'. The other terms are already defined by the Dublin Core Metadata Institute [2].

The following tags are currently provided:
* overheid:authority
* overheid:abbreviation 
* overheid:isRatifiedBy

Credits
------------------------------------------------------------------------------
Sponsored by Provincie Zeeland. [3].
Build by Merge, mvwensen [4].
Thanks to the creators of the Metatag/Metatag DC module [5]. 

References
------------------------------------------------------------------------------
1: http://standaarden.overheid.nl/owms/4.0/doc/NormatievespecificatieOWMS4.0v1.0.1.pdf
2: http://standaarden.overheid.nl/owms
3: http://www.zeeland.nl/
4: https://drupal.org/user/2317482
5: https://drupal.org/project/metatag

