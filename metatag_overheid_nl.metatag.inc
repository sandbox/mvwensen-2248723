<?php
/**
 * @file
 * Metatag integration for the metatag_overheid_nl module.
 */

/**
 * Implements hook_metatag_config_default_alter().
 */
function metatag_overheid_nl_metatag_config_default_alter(array &$configs) {
  foreach ($configs as &$config) {
    switch ($config->instance) {
      case 'node':
        $config->config += array(
          'overheid.authority' => array('value' => ''),
        );
        break;
    }
  }
}

/**
 * Implements hook_metatag_info().
 * Overheid NL Elements taken from http://standaarden.overheid.nl/owms.
 */
function metatag_overheid_nl_metatag_info() {
  $info['groups']['overheid-nl'] = array(
    'label' => t('Overheid NL'),
    'description' => t('The Overheid NL <a href="http://standaarden.overheid.nl/owms" target="_blank" title="Dutch governement OWMS website">OWMS 4.0 Metadata Standard</a> set is created by the Dutch government and is based on the <a href="http://dublincore.org/">Dublin Core Metadata Initiative (DCMI)</a>. Terms in this group are added by the dutch government (prefixed by &quot;overheid:&quot;) next to the existing Dublin Core Terms.'),
    'form' => array(
      '#weight' => 70,
    ),
  );
  $info['tags']['overheid.authority'] = array(
    'label' => t('Overheid Authority'),
    'description' => t('The overheid:authority is the government agency which is legaly responsible and has the ultimate responsibility for the content and tendency of the information concerned.'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'overheid-nl',
    'element' => array(
      '#type' => 'term',
      '#theme' => 'metatag_overheid_nl',
    ),
  );
  $info['tags']['overheid.abbreviation'] = array(
    'label' => t('Overheid Abbreviation (Optional)'),
    'description' => t('The overheid:abbreviation is the formal abbreviation of this content. (Optional)'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'overheid-nl',
    'element' => array(
      '#type' => 'term',
      '#theme' => 'metatag_overheid_nl',
    ),
  );
  $info['tags']['overheid.isRatifiedBy'] = array(
    'label' => t('Overheid Is ratified by (Optional)'),
    'description' => t('The overheid:isRatifiedBy the government agency or the mandated official that ratified this content (Optional).'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'overheid-nl',
    'element' => array(
      '#type' => 'term',
      '#theme' => 'metatag_overheid_nl',
    ),
  );

  return $info;
}